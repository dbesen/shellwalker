
Shellwalker v0.1
-----------------

Shellwalker is a single-player terminal-based video game.

The basic idea is that you can move throughout a 2d virtual world.  When you run the game, you get a top-down view of the landscape around you.

As maps are stored in plain text, they are easily editable.
