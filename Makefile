# Docs on the weird prune syntax: https://www.theunixschool.com/2012/07/find-command-15-examples-to-exclude.html
PY_FILES := $(shell find . -type d -name .venv -prune -o -name '*.py' -print)

SHELL := /bin/bash

.DEFAULT_GOAL := .tested

.venv: pip-requires test-requires
	python3 -m venv .venv
	source .venv/bin/activate && pip install -r test-requires
	source .venv/bin/activate && pip install -r pip-requires

.tested: .venv $(PY_FILES)
	source .venv/bin/activate && nosetests -s --with-xcoverage --cover-package=shellwalker --cover-min-percentage=100
	touch .tested

.packaging_tested:
	./test-packaging.sh
	touch .packaging_tested

.PHONY: clean
clean:
	git clean -Xdff

.PHONY: would-clean
would-clean:
	git clean -Xdn

.PHONY: clean-partial
clean-partial:
	git clean -Xdf -e '!.venv'

.PHONY: test-packaging
test-packaging: .packaging_tested
