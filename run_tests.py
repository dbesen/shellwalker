import os
import nose
import sys

nose_args = [sys.argv[0], '-s', '--with-xcoverage', '--cover-package=shellwalker', '--cover-min-percentage=100']

result = nose.run(argv=nose_args)

sys.exit(not result)
