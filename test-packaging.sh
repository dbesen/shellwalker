#!/bin/bash

set -e
set -x

cd "$( dirname "${BASH_SOURCE[0]}" )"

make clean

# Test: the self-test passes when run as a developer
./start_game --self-test

make clean

# Test: metadata fields should exist

python3 setup.py --author | grep David
python3 setup.py --author-email | grep gmail
python3 setup.py --description | grep -i game
python3 setup.py --long-description | grep -i shellwalker
python3 setup.py --version | grep -v undefined

# Do the build
python3 setup.py sdist

# For debug
echo "Tar includes:"
tar tvfz dist/*.tar.gz
echo

python3 -m venv .test-venv
source .test-venv/bin/activate

pip install dist/shellwalker-*.tar.gz

cd .test-venv # to ensure we're not loading anything from the source tree

# Test: the self-test passes when run after installation
shellwalker --self-test
