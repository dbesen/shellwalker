import curses
from . import game
from . import teleport_manager
from . import coordinate

class Player(coordinate.Coordinate):
    def __init__(self):
        super(Player, self).__init__(None, 0, 0)
        self.last_direction = None
        self.teleport_manager = teleport_manager

    def attach_to_game(self, mygame):
        self.mygame = mygame
        mygame.register_callback('frame', self.frame_callback)
        mygame.register_callback('keystroke', self.key_callback)
        self.map = mygame.worldmap.current_map

    def frame_callback(self):
        attrs = 0
        if self.get_map_char() == '_':
            attrs = curses.A_UNDERLINE
        self.mygame.render(self.x, self.y, "@", attrs)

    def key_callback(self, key):
        dest = self
        if(key == ord('w') or key == curses.KEY_UP):
            dest = coordinate.Coordinate(self.map, self.x, self.y-1)
            self.last_direction = game.DIRECTION_UP
        if(key == ord('s') or key == curses.KEY_DOWN):
            dest = coordinate.Coordinate(self.map, self.x, self.y+1)
            self.last_direction = game.DIRECTION_DOWN
        if(key == ord('a') or key == curses.KEY_LEFT):
            dest = coordinate.Coordinate(self.map, self.x-1, self.y)
            self.last_direction = game.DIRECTION_LEFT
        if(key == ord('d') or key == curses.KEY_RIGHT):
            dest = coordinate.Coordinate(self.map, self.x+1, self.y)
            self.last_direction = game.DIRECTION_RIGHT
        self.mygame.move_with_teleport(self, dest, watch=True)

    def get_last_direction(self):
        return self.last_direction
