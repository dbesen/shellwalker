import curses
import time
import textwrap
from . import viewport
from . import player
from . import world_map
from collections import defaultdict

DESIRED_FPS = 16.0
TIME_PER_FRAME = 1.0/DESIRED_FPS

DIRECTION_UP = 1
DIRECTION_DOWN = 2
DIRECTION_LEFT = 3
DIRECTION_RIGHT = 4

class Game:

    def __init__(self, map=world_map.WorldMap(), player=player.Player()):
        self.stdscr = None
        self.callbacks = defaultdict(list)
        self.playing = False
        self.elapsed_time_last_frame = 0
        self.last_frame_time = time.time()
        self.start_time = self.last_frame_time

        self.worldmap = map
        self.player = player
        self.center_on_player = True

        self.worldmap.attach_to_game(self)
        self.player.attach_to_game(self)

    def isRunning(self):
        return (self.stdscr != None)

    def start(self):
        self.player.set_position(self.worldmap.get_initial_player_position())
        self.stdscr = curses.initscr()
        self.viewport = viewport.Viewport(self.stdscr)
        curses.noecho()
        curses.cbreak()
        self.stdscr.keypad(1)
        curses.start_color()
        curses.curs_set(0)
        self.stdscr.nodelay(1) # makes getch() and getkey() non-blocking
        # curses.raw() # Disables ^c

    def stop(self):
        curses.curs_set(1)
        self.stdscr.keypad(0)
        curses.echo()
        curses.nocbreak()
        curses.endwin()
        self.stdscr = None
        # curses.noraw()

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, type, value, traceback):
        self.stop()

    def render(self, x, y, content, attributes=None):
        x = x - self.viewport.x
        y = y - self.viewport.y
        if y < 0:
            return
        if x < 0:
            content = content[-x:]
            x = 0
        if len(content) < 1:
            return
        screensize = self.stdscr.getmaxyx()
        content = content[:screensize[1] - x]
        if x >= screensize[1] or y >= screensize[0]:
            return
        # print "addstr(" + str(y) + ", " + str(x) + ", " + str(content) + ")"
        if attributes:
            self.stdscr.addstr(y, x, content, attributes)
        else:
            self.stdscr.addstr(y, x, content)

    def stretch_string(self, string, width):
        return string[0] + (string[1] * (width-2)) + string[2]

    def render_box(self, x, y, width, height):
        self.render(x, y, self.stretch_string("/-\\", width))
        for i in range(1, height-1):
            self.render(x, y+i, self.stretch_string("| |", width))
        self.render(x, y+height-1, self.stretch_string("\-/", width))

    def find_longest_length(self, lines):
        length = 0
        for line in lines:
            l = len(line)
            length = max(l, length)
        return length

    def render_textbox(self, x, y, text, maxwidth=0):
        if maxwidth == 0:
            lines = text.splitlines()
        else:
            lines = textwrap.wrap(text, maxwidth)
        longest_line_length = self.find_longest_length(lines)
        self.render_box(x, y, longest_line_length + 4, len(lines) + 2)
        for (i, line) in enumerate(lines):
            self.render(x+2, y+1+i, line)

    def get_character(self, x, y):
        return chr(self.get_raw_character(x, y) & 0xff)

    def get_raw_character(self, x, y):
        # note the y and x are reversed here
        return self.stdscr.inch(y, x)

    def get_string(self, x, y, n):
        return self.stdscr.instr(y, x, n).decode('utf-8')

    def zero_top_left(self):
        self.center_on_player = False
        self.viewport.x = 0
        self.viewport.y = 0
        self.advance_one_frame() # so it clears

    def center_viewport(self):
        if(self.center_on_player):
            self.viewport.center_on(self.player)

    def advance_one_frame(self):
        self.stdscr.clear()

        self.handle_keystroke_callbacks()
        # Keystroke callbacks can cause the window to need to be recentered.
        # So, we recenter the viewport after calling them, and before we actually render.
        self.center_viewport()
        self.fire_callbacks('frame')

        self.stdscr.refresh()
        self.handle_frame_timing()

    def handle_keystroke_callbacks(self):
        while True:
            char = self.stdscr.getch()
            if char == curses.ERR:
                break
            self.fire_callbacks('keystroke', char)

    def fire_callbacks(self, callback_type, *args, **kwargs):
        for callback in self.callbacks[callback_type]:
            callback(*args, **kwargs)

    def handle_frame_timing(self):
        frame_end_time = time.time()
        self.elapsed_time_last_frame = frame_end_time - self.last_frame_time
        self.last_frame_time = frame_end_time

        if(self.elapsed_time_last_frame < TIME_PER_FRAME):
            time_diff = TIME_PER_FRAME - self.elapsed_time_last_frame
            time.sleep(time_diff)
            self.elapsed_time_last_frame = TIME_PER_FRAME
            self.last_frame_time = self.last_frame_time + time_diff

    def play(self):
        self.playing = True
        while(self.playing):
            self.advance_one_frame()

    def register_callback(self, callback_type, callback):
        self.callbacks[callback_type].append(callback)

    def pause(self):
        self.playing = False

    def get_elapsed_since_last_frame(self):
        return self.elapsed_time_last_frame

    def get_time_since_start(self):
        return self.last_frame_time - self.start_time

    def move_with_teleport(self, thing_to_move, destination, watch=False):
        """ If watch, update the world map to display the target map. """
        self.worldmap.move_with_teleport(thing_to_move, destination, watch)
