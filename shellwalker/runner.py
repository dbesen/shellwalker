from . import game
from . import world_map
import argparse

def begin_play(game=game.Game):
    parser = argparse.ArgumentParser()
    parser.add_argument('map', default='tutorial',
        help="Either the name of a built-in map, or the name of a folder on disk to load the map from", nargs='?')
    parser.add_argument('--self-test', action='store_true', help=argparse.SUPPRESS)

    try:
        args = parser.parse_args()
    except SystemExit:
        return

    m = world_map.WorldMap("maps/%s/" % (args.map))
    with game(map=m) as mygame:
        if args.self_test:
            mygame.advance_one_frame()
        else:
            mygame.play()

