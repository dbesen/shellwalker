
from .conversation import Conversation

class Npc(object):
    def __init__(self, number=None, conversation=None, file=None):
        self.number = number
        self.conversation = conversation

        if file is not None:
            with open(file) as f:
                self.conversation = Conversation(input_text=f.read())
