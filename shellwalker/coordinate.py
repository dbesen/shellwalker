from . import positional_object

class Coordinate(positional_object.PositionalObject):
    def __init__(self, map, x, y):
        self.map = map
        super(Coordinate, self).__init__(x, y)

    def __hash__(self):
        return hash((self.map, self.x, self.y))

    def __str__(self):
        return super(Coordinate, self).__str__() + " (Coordinate)"

    def __eq__(self, other):
        return hash(self) == hash(other)

    def get_map_char(self):
        return self.map.get_map_char(self.x, self.y)

    def can_move_to(self, destination):
        char = destination.get_map_char()
        if(char != ' ' and char != '_'):
            return False

        if destination.map == self.map:
            if destination.x == self.x:
                # Check if we're on an underscore and moving down
                if destination.y == self.y + 1:
                    if self.get_map_char() == '_':
                        return False
                # Check if we're below an underscore and moving up
                if destination.y == self.y - 1:
                    if destination.get_map_char() == '_':
                        return False

        return True

    def move_to(self, destination):
        if self.can_move_to(destination):
            self.set_position(destination)
            self.map = destination.map

