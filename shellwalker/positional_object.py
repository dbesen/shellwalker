
class PositionalObject(object):

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def set_position(self, po):
        self.x = po.x
        self.y = po.y

    # We can't just set x and y defaults in the __init__ function since the
    # classes that override this one may not call that.
    # So, we make a property just to set the default value.
    @property
    def x(self):
        if not hasattr(self, '_x'):
            self._x = 0
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    @property
    def y(self):
        if not hasattr(self, '_y'):
            self._y = 0
        return self._y

    @y.setter
    def y(self, value):
        self._y = value

    def __str__(self):
        return "(" + str(self.x) + ", " + str(self.y) + ")"

    def __repr__(self):
        return "PositionalObject" + str(self)
