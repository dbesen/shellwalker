from collections import defaultdict

class TeleportManager(object):
    def __init__(self):
        self.teleporters_by_char = defaultdict(list)
        self.needs_recalculate = True

    def add_teleport(self, key, teleport_coordinate):
        self.teleporters_by_char[key].append(teleport_coordinate)
        self.needs_recalculate = True

    def recalculate(self):
        if not self.needs_recalculate:
            return
        self.teleporters = defaultdict(lambda: None)
        # Rework teleporters array to make lookups easier
        for key, item in self.teleporters_by_char.items():
            num = len(item)
            for i in range(0, num):
                self.teleporters[item[i]] = item[(i+1)%num]
        self.needs_recalculate = False

    def teleports_to(self, teleport_coordinate):
        self.recalculate()
        result = self.teleporters[teleport_coordinate]
        if result is not None:
            return result
        return teleport_coordinate

    def move_with_teleport(self, thing_to_move, destination):
        dest = self.teleports_to(destination)
        thing_to_move.move_to(dest)
