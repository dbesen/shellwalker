from . import positional_object

def get_center(x, y):
    if x is None or y is None:
        return None

    if hasattr(x, 'x'):
        # x and y are PositionalObjects that have their own x and y
        return positional_object.PositionalObject(get_center(x.x, y.x), get_center(x.y, y.y))
    else:
        return (x + y) // 2

def get_upper_left_for_center(position, width, height):
    return positional_object.PositionalObject(position.x - width//2, position.y - height//2)

class Viewport(positional_object.PositionalObject):

    def __init__(self, stdscr):
        self.stdscr = stdscr

    def move_to(self, x, y=None):
        if y:
            self.x = x
            self.y = y
        else: # We were given a positional object argument
            self.x = x.x
            self.y = x.y

    def center_on(self, position):
        screen_size = self.stdscr.getmaxyx()
        self.move_to(get_upper_left_for_center(position, screen_size[1], screen_size[0]))
