
class Answer(object):
    def __init__(self, lines, indent_level):
        self.text = lines[0][indent_level:]
        self.parse_targets(lines[1:], indent_level)

    def parse_targets(self, lines, indent_level):
        self.target = Conversation(split_lines=lines, indent_level=indent_level)
        if self.target.get_num_questions() == 0:
            self.target = None

    def get_text(self):
        return self.text

    def get_target(self):
        return self.target

class Question(object):
    def __init__(self, lines, indent_level):
        self.answers = []
        self.parse(lines, indent_level)

    def parse(self, lines, indent_level):
        self.text = lines[0][indent_level:]

    def get_text(self):
        return self.text

    def add_answer(self, answer):
        self.answers.append(answer)

    def get_answer_count(self):
        return len(self.answers)

    def get_answer_object(self, index):
        return self.answers[index]

class Conversation(object):
    def __init__(self, input_text=None, split_lines=None, indent_level=0):
        if split_lines is None:
            split_lines = input_text.splitlines()
        self.questions = self.parse(split_lines, indent_level)
        if len(self.questions) > 0:
            self.current_question = self.questions[0]
        self.current_list_index = 0

    def get_indent_level(self, line):
        count = 0
        for i in range(0, len(line)):
            if line[i] == ' ':
                count = count + 1
            else:
                break
        return count

    def is_answer(self, line, indent_level):
        return line[indent_level] == '>'

    def parse(self, lines, indent_level):
        questions = []
        for (i, line) in enumerate(lines):
            if not self.get_indent_level(line) == indent_level:
                continue
            if self.is_answer(line, indent_level):
                questions[-1].add_answer(Answer(lines[i:], indent_level+2))
            else:
                questions.append(Question(lines[i:], indent_level))
        return questions

    def get_num_questions(self):
        return len(self.questions)

    def get_text(self):
        return self.current_question.get_text()

    def get_answer_count(self):
        return self.current_question.get_answer_count()

    def get_answer_object(self, answer_index):
        return self.current_question.get_answer_object(answer_index)

    def get_answer(self, answer_index):
        return self.get_answer_object(answer_index).get_text()

    def advance_question(self):
        self.current_list_index += 1
        self.current_question = self.questions[self.current_list_index]

    def select_response(self, answer_index):
        self.current_question = self.get_answer_object(answer_index).get_target()
        if self.current_question is None:
            self.advance_question()

