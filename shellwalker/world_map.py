from . import positional_object
from . import teleport_manager
import os
import re
import pkg_resources
from . import coordinate

class IndividualMap(object):
    def __init__(self, str):
        self.mapstr = str
        self.display_mapstr = str

        # Display NPCs without the angle brackets
        self.display_mapstr = re.sub("<(.)>", " \\1 ", self.display_mapstr)

    def callback_teleporters(self, callback):
        # Invokes the 'callback' method over and over with two args: char, and Coordinate
        self.display_mapstr = re.sub('\[.\]', '[ ]', self.display_mapstr)
        y = 0
        for line in self.mapstr.splitlines():
            for match in re.finditer('\[(.)\]', line):
                x = match.start(1)
                char = match.group(1)
                callback(char, coordinate.Coordinate(self, x, y))
            y = y + 1

    def get_initial_player_position(self):
        self.display_mapstr = self.display_mapstr.replace("@", " ")
        i = 0
        for line in self.mapstr.splitlines():
            xpos = line.find("@")
            if xpos != -1:
                return positional_object.PositionalObject(xpos, i)
            i = i + 1
        return None

    def render(self, mygame, x, y):
        i = y
        for line in self.display_mapstr.splitlines():
            mygame.render(x, i, line)
            i = i + 1

    def get_map_char(self, x, y):
        if x < 0 or y < 0:
            return ' '
        lines = self.display_mapstr.splitlines()
        if y >= len(lines):
            return ' '
        line = lines[y]
        if x >= len(line):
            return ' '
        return line[x]


class WorldMap(positional_object.PositionalObject):
    def __init__(self, mapfolder=None, mapstr=None):
        self.current_map = None
        self.initial_player_position = positional_object.PositionalObject(0, 0)
        self.teleport_manager = teleport_manager.TeleportManager()
        if not mapstr:
            mapstr = ""
        self.loadFromString(mapstr)
        if mapfolder:
            self.loadFromFolder(mapfolder)

    def loadFromFolder(self, folder):
        if not os.path.isdir(folder):
            folder = pkg_resources.resource_filename(__name__, folder)
        self.loaded_from = folder
        for file in os.listdir(folder):
            path = os.path.join(folder, file)
            if os.path.isfile(path):
                with open(path) as f:
                    self.loadFromString(f.read())

    def loadFromString(self, str):
        i = IndividualMap(str)
        self.addIndividualMap(i)

    def addIndividualMap(self, map_to_add):

        # In some of the unit tests, we create a single map with no marked player position.
        # In that case, we want to set current_map so it's not None.
        if self.current_map is None:
            self.current_map = map_to_add

        initial_position = map_to_add.get_initial_player_position()
        if initial_position is not None:
            self.initial_player_position = initial_position
            self.current_map = map_to_add
        map_to_add.callback_teleporters(self.teleport_manager.add_teleport)

    def attach_to_game(self, mygame):
        mygame.register_callback('frame', self.frame_callback)
        self.mygame = mygame

    def frame_callback(self):
        if self.current_map is not None:
            self.current_map.render(self.mygame, self.x, self.y)

    def get_initial_player_position(self):
        return self.initial_player_position

    def move_with_teleport(self, thing_to_move, destination, watch=False):
        self.teleport_manager.move_with_teleport(thing_to_move, destination)
        if watch:
            self.current_map = thing_to_move.map
