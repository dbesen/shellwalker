import unittest2
from pyflakes import api as pyflakes_api

class TestPyflakes(unittest2.TestCase):
    def test_no_pyflakes_issues(self):
        warnings = pyflakes_api.checkRecursive([ 'shellwalker', 'tests' ], None)
        self.assertEquals(0, warnings)
