import test_base
import mock
import shellwalker.game as game
import curses

class TestTestBase(test_base.TestBase):
    def test_assert_instanceof(self):
        self.assertInstanceOf(mock.Mock, mock.Mock())

    def test_assert_instanceof_fail(self):
        with self.shouldFail():
            self.assertInstanceOf(mock.Mock, 3)

    def test_assert_not_none(self):
        self.assertNotNone(self)

    def test_assert_not_none_fail(self):
        with self.shouldFail():
            self.assertNotNone(None)

    def test_assert_same(self):
        self.assertSame(1, 1)
        self.assertSame("a", "a")
        class Thing(object):
            pass
        a = Thing()
        b = Thing()
        self.assertSame(a, a)

        with self.shouldFail():
            self.assertSame(a, b)

    def test_with_exceptions(self):
        with self.shouldFail():
            raise AssertionError()

    def test_with_exceptions_fail(self):
        try:
            with self.shouldFail():
                pass
        except AssertionError:
            pass
        else:
            self.fail("Expected an AssertionError")

    def test_with_exception_different_type(self):
        with self.shouldFail(type=AttributeError):
            raise AttributeError()

    def test_with_exception_wrong_type(self):
        try:
            with self.shouldFail(type=AttributeError):
                raise AssertionError()
        except AssertionError:
            pass
        else:
            self.fail("Expected an AssertionError")

    def test_contains(self):
        self.assert_contains("asdf", "sd")

    def test_contains_fail(self):
        with self.shouldFail():
            self.assert_contains("asdf", "jkl")

    def test_assert_character(self):
        with game.Game() as mygame:
            mygame.render(1, 1, "a")
            mygame.render(2, 2, "b")
            self.assert_character(mygame, 1, 1, "a")
            self.assert_character(mygame, 2, 2, "b")
            with self.shouldFail():
                self.assert_character(mygame, 1, 1, "b")

    def test_assert_has_attributes(self):
        with game.Game() as mygame:
            mygame.render(1, 1, "a")
            mygame.render(2, 2, "b", curses.A_UNDERLINE)
            mygame.render(3, 3, "c", curses.A_UNDERLINE | curses.A_BOLD)
            self.assert_has_attributes(mygame, 1, 1, 0)
            self.assert_has_attributes(mygame, 2, 2, curses.A_UNDERLINE)
            self.assert_has_attributes(mygame, 3, 3, curses.A_UNDERLINE)
            self.assert_has_attributes(mygame, 3, 3, curses.A_BOLD)
            with self.shouldFail():
                self.assert_has_attributes(mygame, 1, 1, curses.A_UNDERLINE)

    def test_assert_does_not_have_attributes(self):
        with game.Game() as mygame:
            mygame.render(1, 1, "a")
            mygame.render(2, 2, "b", curses.A_UNDERLINE)
            mygame.render(3, 3, "c", curses.A_UNDERLINE | curses.A_BOLD)
            self.assert_does_not_have_attributes(mygame, 1, 1, curses.A_UNDERLINE)
            self.assert_does_not_have_attributes(mygame, 1, 1, curses.A_BOLD)
            self.assert_does_not_have_attributes(mygame, 1, 1, curses.A_UNDERLINE | curses.A_BOLD)
            self.assert_does_not_have_attributes(mygame, 1, 2, curses.A_BOLD)

            with self.shouldFail():
                self.assert_does_not_have_attributes(mygame, 3, 3, curses.A_UNDERLINE)

            with self.shouldFail():
                self.assert_does_not_have_attributes(mygame, 3, 3, curses.A_BOLD)

