import shellwalker.runner
import sys
import test_base
import mock
from io import StringIO
import os
from contextlib import contextmanager
from unittest.mock import patch

class EasyStringIO(StringIO):
    def __contains__(self, key):
        return key in self.getvalue()

    def __str__(self):
        return self.getvalue()

@contextmanager
def CaptureOutput(argv=[]):
    argv = ["program name"] + argv

    argv_save = sys.argv
    stdout_save = sys.stdout
    stderr_save = sys.stderr

    sys.argv = argv
    sys.stdout = EasyStringIO()
    sys.stderr = EasyStringIO()

    yield (sys.stdout, sys.stderr)

    sys.stdout = stdout_save
    sys.stderr = stderr_save
    sys.argv = argv_save

class TestCaptureOutput(test_base.TestBase):
    def test_capture(self):
        with CaptureOutput() as (stdout, stderr):
            print("asdf")
            self.assert_contains(stdout, "asdf")
            print("jkl")
            self.assert_contains(stdout, "jkl")
            sys.stderr.write("foo")
            self.assert_contains(stderr, "foo")
            sys.stderr.write("bar")
            self.assert_contains(stderr, "bar")

    def method(self):
        print("1234")

    def test_capture_method(self):
        with CaptureOutput() as (stdout, stderr):
            self.method()
            self.assert_contains(stdout, "1234")

    def test_replace_argv(self):
        argv_save = sys.argv
        with CaptureOutput(["arg1", "arg2"]):
            self.assertEquals(["program name", "arg1", "arg2"], sys.argv)
            self.assertNotEquals(argv_save, sys.argv)

        # Ensure we reset afterwards
        self.assertEquals(argv_save, sys.argv)

class TestRunner(test_base.TestBase):
    def test_play_is_called(self):
        with patch.object(sys, 'argv', ['shellwalker']):
            mock_game = shellwalker.game.Game
            mock_game.play = mock.Mock()
            shellwalker.runner.begin_play(game=mock_game)
            mock_game.play.assert_called_once_with()

    def assert_shows_usage(self, args, check_stdout=True):
        with CaptureOutput(args) as (stdout, stderr):
            mock_game = shellwalker.game.Game
            mock_game.play = mock.Mock()
            shellwalker.runner.begin_play(game=mock_game)
            mock_game.play.assert_not_called()
            if check_stdout:
                self.assert_contains(stdout, "usage")
            else:
                self.assert_contains(stderr, "usage")

    def assert_paths_equal(self, path1, path2):
        self.assertEquals(os.path.abspath(path1), os.path.abspath(path2))

    def assert_loads_map(self, expected, args):
        with CaptureOutput(args):
            mock_game = mock.MagicMock()
            shellwalker.runner.begin_play(game=mock_game)
            expected = 'shellwalker/maps/' + expected
            self.assert_paths_equal(expected, mock_game.mock_calls[0][2]['map'].loaded_from)

    def test_usage(self):
        self.assert_shows_usage([ "arg1", "arg2" ], check_stdout=False)
        self.assert_shows_usage([ "--help" ])
        self.assert_shows_usage([ "arg1", "arg2", "arg3" ], check_stdout=False)

    def test_self_test_not_in_usage(self):
        with CaptureOutput(["--help"]) as (stdout, stderr):
            mock_game = shellwalker.game.Game
            mock_game.play = mock.Mock()
            shellwalker.runner.begin_play(game=mock_game)
            self.assertFalse('self' in stdout)
            self.assertFalse('self' in stderr)

    def test_map_arg_description(self):
        with CaptureOutput(["--help"]) as (stdout, stderr):
            mock_game = shellwalker.game.Game
            mock_game.play = mock.Mock()
            shellwalker.runner.begin_play(game=mock_game)
            self.assert_contains(stdout, "load")

    def test_map_loading(self):
        self.assert_loads_map("tutorial", [ "tutorial" ])
        self.assert_loads_map("1", [ "1" ])
        self.assert_loads_map("tutorial", [ ])
        self.assert_loads_map("tutorial", [ "--self-test" ])
        self.assert_loads_map("tutorial", [ "tutorial", "--self-test" ])
        self.assert_loads_map("tutorial", [ "--self-test", "tutorial" ])
        self.assert_loads_map("1", [ "1", "--self-test" ])
        self.assert_loads_map("1", [ "--self-test", "1" ])

    def test_load_from_cwd(self):
        pass # not written yet
        # with TestMap?
        # assert_loads_from_cwd? or what? right now shellwalker/maps/ is hardcoded in assert_loads_map
        # self.assert_loads_map("map-in-cwd", [ "map-in-cwd" ])
        # self.assert_loads_map("map-in-cwd", [ "--self-test", "map-in-cwd" ])
        # self.assert_loads_map("map-in-cwd", [ "map-in-cwd", "--self-test" ])

    def test_self_test(self): # lol
        with CaptureOutput(["--self-test"]):
            shellwalker.runner.begin_play()
            # We should exit, with no exceptions
