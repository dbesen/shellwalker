import test_base
from shellwalker import npc, conversation, world_map, game
import os
import tempfile
from contextlib import contextmanager

@contextmanager
def tempFile(file_contents):
    f = tempfile.NamedTemporaryFile(delete=False)
    path = f.name
    f.write(file_contents.encode('utf-8'))
    f.close()
    yield path
    os.unlink(path)

class TestNpc(test_base.TestBase):
    def test_npc_constructor(self):
        c = conversation.Conversation("message")
        n = npc.Npc(1, c)
        self.assertEquals("message", n.conversation.get_text())
        self.assertEquals(1, n.number)

    def test_load_from_file(self):
        with tempFile("this is in the npc file") as filename:
            n = npc.Npc(file=filename)
            self.assertEquals("this is in the npc file", n.conversation.get_text())

    def testNpcDisplay(self):
        m = world_map.WorldMap()
        m.loadFromString(" @ <3>")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 0, '@')
            self.assert_string(mygame, 2, 0, "  3 ")

    def testNpcDisplayDifferent(self):
        m = world_map.WorldMap()
        m.loadFromString(" @ <2>")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 0, '@')
            self.assert_string(mygame, 2, 0, "  2 ")
