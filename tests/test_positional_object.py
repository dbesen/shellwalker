import test_base
import shellwalker.positional_object as positional_object

class PositionalObjectTest(test_base.TestBase):

    def test_has_x_y(self):
        p = positional_object.PositionalObject()
        self.assertEquals(0, p.x)
        self.assertEquals(0, p.y)

    def test_has_x_y_inherited(self):
        class PositionalTest(positional_object.PositionalObject):
            def __init__(self):
                pass
        p = PositionalTest()
        self.assertEquals(0, p.x)
        self.assertEquals(0, p.y)

    def test_construct_with_values(self):
        p = positional_object.PositionalObject(1, 2)
        self.assertEquals(1, p.x)
        self.assertEquals(2, p.y)

    def test_set_position(self):
        source = positional_object.PositionalObject(1, 2)
        target = positional_object.PositionalObject(3, 4)

        target.set_position(source)

        self.assertEquals(1, target.x)
        self.assertEquals(2, target.y)
        self.assertEquals(1, source.x)
        self.assertEquals(2, source.y)

    def test_to_str(self):
        a = positional_object.PositionalObject(1, 2)
        self.assertEquals("(1, 2)", str(a))
        self.assertEquals("PositionalObject(1, 2)", repr(a))
