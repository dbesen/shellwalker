import test_base
import shellwalker.game as game
import mock

class TestBootstrapping(test_base.TestBase):
    # Use cases:
    # Start the game for real, with a real map and real player, in a with block.
    # Start the game for test, with a mock map and mock player, in a with block.
    # Real map/fake player, real player/fake map.

    def test_both_mock(self):
        p = mock.Mock()
        m = mock.Mock()
        with game.Game(player=p, map=m) as mygame:
            self.assertEquals(p, mygame.player)
            self.assertEquals(m, mygame.worldmap)

    def test_neither_mock(self):
        with game.Game() as mygame:
            self.assertNotNone(mygame.player)
            self.assertNotNone(mygame.worldmap)
