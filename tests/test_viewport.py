import test_base

import shellwalker.game as game
import shellwalker.player as player
import shellwalker.viewport as viewport
import shellwalker.world_map as world_map
import shellwalker.positional_object as positional_object
from mock import Mock

class ViewportTest(test_base.TestBase):
    def test_move_to(self):
        p = player.Player()
        with game.Game(player=p) as mygame:
            mygame.zero_top_left()
            p.x = 10
            p.y = 10
            mygame.advance_one_frame()
            self.assert_character(mygame, 10, 10, "@")
            mygame.viewport.move_to(10, 10)
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, "@")

    def test_glide_to(self):
        pass # TODO

    def test_move_to_object(self):
        with game.Game() as mygame:
            p = positional_object.PositionalObject()
            p.x = 10
            p.y = 10

            mygame.viewport.move_to(p)

            self.assertEquals(mygame.viewport.x, 10)
            self.assertEquals(mygame.viewport.y, 10)

    def test_centering_one_dimension(self):
        self.assertEquals(1, viewport.get_center(0, 2))
        self.assertEquals(2, viewport.get_center(0, 4))
        self.assertEquals(3, viewport.get_center(1, 5))
        self.assertEquals(3, viewport.get_center(5, 1))
        self.assertEquals(1, viewport.get_center(1, 1))
        self.assertEquals(0, viewport.get_center(0, 0))
        self.assertEquals(0, viewport.get_center(-1, 1))
        self.assertEquals(0, viewport.get_center(1, -1))
        self.assertEquals(-2, viewport.get_center(-1, -3))
        self.assertEquals(-2, viewport.get_center(-3, -1))
        self.assertEquals(None, viewport.get_center(None, None))
        self.assertEquals(None, viewport.get_center(None, 1))
        self.assertEquals(None, viewport.get_center(1, None))
        self.assertEquals(1, viewport.get_center(1.1, 1.3))
        self.assertEquals(1, viewport.get_center(1, 2))
        self.assertEquals(0, viewport.get_center(0, 1))

    def assert_center(self, ob1, ob2, x, y):
        self.assertEquals(x, viewport.get_center(ob1, ob2).x)
        self.assertEquals(y, viewport.get_center(ob1, ob2).y)

    def test_centering_two_dimensions_two_objects(self):
        topleft = positional_object.PositionalObject(0, 1)
        bottomright = positional_object.PositionalObject(2, 3)
        self.assert_center(topleft, bottomright, 1, 2)
        self.assert_center(bottomright, topleft, 1, 2)

    def test_centering_two_dimensions_two_objects_other_diagonal(self):
        topright = positional_object.PositionalObject(0, 3)
        bottomleft = positional_object.PositionalObject(2, 1)
        self.assert_center(topright, bottomleft, 1, 2)
        self.assert_center(bottomleft, topright, 1, 2)

    def assertPosition(self, x, y, position):
        self.assertEquals(x, position.x)
        self.assertEquals(y, position.y)

    def assertUpperLeftForCenter(self, objx, objy, width, height, expected_x, expected_y):
        result = viewport.get_upper_left_for_center(positional_object.PositionalObject(objx, objy), width, height)
        self.assertPosition(expected_x, expected_y, result)

    def test_centering_reverse(self):
        self.assertUpperLeftForCenter(1, 1, 3, 3, 0, 0)
        self.assertUpperLeftForCenter(5, 5, 3, 3, 4, 4)
        self.assertUpperLeftForCenter(5, 10, 3, 5, 4, 8)

        self.assertUpperLeftForCenter(1, 1, 2, 2, 0, 0)

    def test_real_centering(self):
        m = world_map.WorldMap(mapstr='@\n #')
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.viewport.stdscr = Mock(wraps=mygame.stdscr)
            mygame.viewport.stdscr.getmaxyx = Mock(return_value = (2, 2))

            mygame.advance_one_frame()
            mygame.viewport.center_on(positional_object.PositionalObject(1, 1))
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 1, '#')

    def test_real_centering_larger(self):
        m = world_map.WorldMap(mapstr='@\n\n   #')
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.viewport.stdscr = Mock(wraps=mygame.stdscr)
            mygame.viewport.stdscr.getmaxyx = Mock(return_value = (9, 19)) # 20x10

            mygame.advance_one_frame()
            mygame.viewport.center_on(mygame.player)
            mygame.advance_one_frame()
            self.assert_character(mygame, 9, 4, '@')
            mygame.viewport.center_on(positional_object.PositionalObject(3, 2))
            mygame.advance_one_frame()
            self.assert_character(mygame, 9, 4, '#')

    def test_centering_is_on_by_default(self):
        m = world_map.WorldMap(mapstr='@')
        with game.Game(map=m) as mygame:
            mygame.viewport.stdscr = Mock(wraps=mygame.stdscr)
            mygame.viewport.stdscr.getmaxyx = Mock(return_value = (2, 2))

            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 1, '@')

    def test_turn_off_centering(self):
        m = world_map.WorldMap(mapstr='@')
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')
