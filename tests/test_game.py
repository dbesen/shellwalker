import shellwalker.game as game
import test_base
import mock

class TestGame(test_base.TestBase):

    def test_game_starts_and_stops(self):
        mygame = game.Game()
        self.assert_game_is_not_running(mygame)
        mygame.start()
        self.assert_game_is_running(mygame)
        mygame.stop()
        self.assert_game_is_not_running(mygame)

    def test_game_starts_and_stops_with(self):
        with game.Game() as mygame:
            self.assert_game_is_running(mygame)
        self.assert_game_is_not_running(mygame)

    def assert_no_render(self, inx, iny, instr):
        with game.Game() as mygame:
            mygame.stdscr = mock.Mock()
            mygame.stdscr.addstr = mock.Mock()
            mygame.render(inx, iny, instr)
            self.assertNotCalled(mygame.stdscr.addstr)

    def assert_render(self, inx, iny, instr, outx, outy, outstr):
        with game.Game() as mygame:
            mygame.stdscr = mock.Mock()
            mygame.stdscr.addstr = mock.Mock()
            mygame.stdscr.getmaxyx = mock.Mock(return_value = (100, 100))
            mygame.render(inx, iny, instr)
            mygame.stdscr.addstr.assert_called_once_with(outy, outx, outstr)

    def test_render_negative_coordinates(self):
        self.assert_no_render(0, -1, 'asdf')
        self.assert_render(0, 0, 'asdf', 0, 0, 'asdf')
        self.assert_render(0, 1, 'asdf', 0, 1, 'asdf')
        self.assert_render(1, 0, 'asdf', 1, 0, 'asdf')
        self.assert_render(-1, 0, 'asdf', 0, 0, 'sdf')
        self.assert_render(-3, 0, 'asdf', 0, 0, 'f')
        self.assert_no_render(-4, 0, 'asdf')
