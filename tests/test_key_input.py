import test_base
import shellwalker.game as game
import curses

class TestKeyInput(test_base.TestBase):

    def test_curses_getch_works_non_blocking(self):
        with game.Game() as mygame:
            for i in range(1,100):
                self.assertEquals(curses.ERR, mygame.stdscr.getch())

    def test_one_key_in_frame(self):
        self.last_key_hit = None
        with game.Game() as mygame:
            def callback(key):
                self.last_key_hit = key
            mygame.register_callback('keystroke', callback)
            mygame.advance_one_frame()
            self.assertEquals(None, self.last_key_hit)
            curses.ungetch("i")
            mygame.advance_one_frame()
            self.assertEquals(ord("i"), self.last_key_hit)
            mygame.advance_one_frame()
            self.last_key_hit = None
            mygame.advance_one_frame()
            self.assertEquals(None, self.last_key_hit)

    def test_two_keys_hit_in_one_frame(self):
        self.keys_hit = []
        with game.Game() as mygame:
            def callback(key):
                self.keys_hit.append(key)
            mygame.register_callback('keystroke', callback)
            mygame.advance_one_frame()
            self.assertEquals(0, len(self.keys_hit))
            # Since we're un-getting characters, we do the second one first.
            curses.ungetch("j")
            curses.ungetch("i")
            mygame.advance_one_frame()
            self.assertEquals([ord("i"), ord("j")], self.keys_hit)
