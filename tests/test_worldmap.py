import test_base
import curses
import os
import shutil
from shellwalker import game
from shellwalker import world_map
from shellwalker import coordinate
from contextlib import contextmanager

@contextmanager
def tempMapFile(map_folder, map_filename, map_contents):
    filename = map_folder + '/' + map_filename

    os.mkdir(map_folder)
    with open(filename, 'w') as f:
        f.write(map_contents)

    yield

    shutil.rmtree(map_folder)

class TestWorldMap(test_base.TestBase):

    def testMapRendersOneCharacter(self):
        m = world_map.WorldMap()
        m.loadFromString("@M")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 0, 'M')

    def testMapRendersString(self):
        m = world_map.WorldMap()
        m.loadFromString("@String")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_string(mygame, 1, 0, 'String')

    def testMapRendersStringFails(self):
        # This test is really testing assert_string.  So, it should probably be in test_base.py.
        m = world_map.WorldMap()
        m.loadFromString("@String")
        with game.Game(map=m) as mygame:
            mygame.advance_one_frame()
            with self.shouldFail():
                self.assert_string(mygame, 1, 0, 'Strung')

    def testMapRendersTwoLineString(self):
        m = world_map.WorldMap()
        m.loadFromString("@1\n2")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 0, '1')
            self.assert_character(mygame, 0, 1, '2')

    def testMapRendersTwoLineStringFails(self):
        m = world_map.WorldMap()
        m.loadFromString("@1\n2")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 0, '1')
            with self.shouldFail():
                self.assert_character(mygame, 0, 1, '3')

    def testMapRenderFromFile(self):
        with tempMapFile("test-map-folder", "test-map", "@This came from the test map file"):
            m = world_map.WorldMap("test-map-folder")
            with game.Game(map=m) as mygame:
                mygame.zero_top_left()
                mygame.advance_one_frame()
                self.assert_string(mygame, 1, 0, 'This came from the test map file')

    def testMapDoesNotLoadFolders(self):
        with tempMapFile("test-map-folder", "test-map", "@whatever"):
            os.mkdir('test-map-folder/npcs')
            with open('test-map-folder/npcs/3.txt', 'w') as f:
                f.write("npc text here")
            m = world_map.WorldMap("test-map-folder")
            with game.Game(map=m):
                pass

    def testPlayerStartsAtAtUpperLeft(self):
        m = world_map.WorldMap()
        m.loadFromString("@")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')

    def testPlayerMovementLeavesNoAt(self):
        m = world_map.WorldMap()
        m.loadFromString("@")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')
            curses.ungetch("d")
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, ' ')
            self.assert_character(mygame, 1, 0, '@')

    def testPlayerStartsAtAtOffset(self):
        m = world_map.WorldMap()
        m.loadFromString(" @")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, ' ')
            self.assert_character(mygame, 1, 0, '@')

    def testPlayerStartsAtAtOffsetSecondRow(self):
        m = world_map.WorldMap()
        m.loadFromString("\n@")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, ' ')
            self.assert_character(mygame, 0, 1, '@')
            curses.ungetch("d")
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, ' ')
            self.assert_character(mygame, 0, 1, ' ')
            self.assert_character(mygame, 1, 1, '@')

    def assert_can_move_to(self, mapstr, from_x, from_y, to_x, to_y):
        i = world_map.IndividualMap(mapstr)
        from_coord = coordinate.Coordinate(i, from_x, from_y)
        to_coord = coordinate.Coordinate(i, to_x, to_y)
        self.assertTrue(from_coord.can_move_to(to_coord))

    def assert_cannot_move_to(self, mapstr, from_x, from_y, to_x, to_y):
        i = world_map.IndividualMap(mapstr)
        from_coord = coordinate.Coordinate(i, from_x, from_y)
        to_coord = coordinate.Coordinate(i, to_x, to_y)
        self.assertFalse(from_coord.can_move_to(to_coord))

    def test_can_move_to(self):
        self.assert_can_move_to(' ', 0, 0, 1, 0)
        self.assert_can_move_to(' ', 1, 0, 0, 0)
        self.assert_can_move_to(' ', 0, 0, 0, 1)
        self.assert_can_move_to(' ', 0, 1, 0, 0)
        self.assert_can_move_to(' #', 0, 0, 2, 0) # skips are assumed to be teleports
        self.assert_cannot_move_to(' #', 0, 0, 1, 0)
        self.assert_cannot_move_to('# ', 1, 0, 0, 0)
        self.assert_cannot_move_to(' \n#', 0, 0, 0, 1)

    def testGetMapChar(self):
        i = world_map.IndividualMap('12\n34')

        self.assertEquals('1', i.get_map_char(0, 0))
        self.assertEquals('2', i.get_map_char(1, 0))
        self.assertEquals('3', i.get_map_char(0, 1))
        self.assertEquals('4', i.get_map_char(1, 1))

    def testNegativeMovement(self):
        m = world_map.WorldMap(mapstr='@\nA')
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')
            curses.ungetch("a")
            mygame.advance_one_frame()
            curses.ungetch("s")
            mygame.advance_one_frame()
            curses.ungetch("s")
            mygame.advance_one_frame()
            curses.ungetch("d")
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 2, '@')

    def testMultipleMapsStartingPosition(self):
        m = world_map.WorldMap()
        m.loadFromString(' @\n[2]\nA')
        m.loadFromString('\n[2]\nB')
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 0, '@')
            self.assert_character(mygame, 0, 2, 'A')

    def testMultipleMapsTeleport(self):
        m = world_map.WorldMap()
        m.loadFromString(' @\n[2]\nA')
        m.loadFromString('\n[2]\nB')
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 2, 'A')
            curses.ungetch('s')
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 2, 'B')
