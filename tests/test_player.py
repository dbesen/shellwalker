import test_base

import curses
from shellwalker import game
from shellwalker import player
from shellwalker import world_map

class TestPlayer(test_base.TestBase):
    def testPlayerRenders(self):
        m = world_map.WorldMap(mapstr='@')
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')
            self.assert_character(mygame, 1, 0, ' ')
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')
            self.assert_character(mygame, 1, 0, ' ')
            curses.ungetch("d")
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 0, '@')
            self.assert_character(mygame, 0, 0, ' ')

    def testPlayerMovementWsad(self):
        m = world_map.WorldMap(mapstr='@')
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')

            curses.ungetch("d")
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 0, '@')

            curses.ungetch("s")
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 1, '@')

            curses.ungetch("a")
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 1, '@')

            curses.ungetch("w")
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')

    def testPlayerMovementArrowKeys(self):
        m = world_map.WorldMap(mapstr='@')
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')

            curses.ungetch(curses.KEY_RIGHT)
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 0, '@')

            curses.ungetch(curses.KEY_DOWN)
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 1, '@')

            curses.ungetch(curses.KEY_LEFT)
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 1, '@')

            curses.ungetch(curses.KEY_UP)
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')

    def testLastDirection(self):
        m = world_map.WorldMap(mapstr='@')
        with game.Game(map=m) as mygame:
            p = mygame.player
            self.assertEquals(None, p.get_last_direction())
            p.key_callback(curses.KEY_RIGHT)
            self.assertEquals(game.DIRECTION_RIGHT, p.get_last_direction())
            p.key_callback(curses.KEY_LEFT)
            self.assertEquals(game.DIRECTION_LEFT, p.get_last_direction())
            p.key_callback(curses.KEY_UP)
            self.assertEquals(game.DIRECTION_UP, p.get_last_direction())
            p.key_callback(curses.KEY_DOWN)
            self.assertEquals(game.DIRECTION_DOWN, p.get_last_direction())

    def testPlayerRendersAfterMap(self):
        m = world_map.WorldMap()
        m.loadFromString("m")
        p = player.Player()
        with game.Game(map=m, player=p) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()

            # Ensure they overlap
            self.assertEquals(p.x, m.x)
            self.assertEquals(p.y, m.y)

            # If the map renders last, this will return 'm'.
            self.assert_character(mygame, 0, 0, '@')

    def xtestPlayerRendersAfterMapEvenIfCreatedFirst(self):
        with game.Game() as mygame:
            p = player.Player(mygame)

            m = world_map.WorldMap(mygame)
            m.loadFromString("m")

            mygame.advance_one_frame()

            # Ensure they overlap
            self.assertEquals(p.x, m.x)
            self.assertEquals(p.y, m.y)

            # If the map renders last, this will return 'm'.
            self.assert_character(mygame, 0, 0, '@')

    def testPlayerMovementRestrictionRight(self):
        m = world_map.WorldMap(mapstr="@#")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')
            self.assert_character(mygame, 1, 0, '#')
            curses.ungetch("d")
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')
            self.assert_character(mygame, 1, 0, '#')

    def testPlayerMovementRestrictionLeft(self):
        m = world_map.WorldMap(mapstr="#@")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '#')
            self.assert_character(mygame, 1, 0, '@')
            curses.ungetch("a")
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '#')
            self.assert_character(mygame, 1, 0, '@')

    def testPlayerMovementRestrictionDown(self):
        m = world_map.WorldMap(mapstr="@\n#")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')
            self.assert_character(mygame, 0, 1, '#')
            curses.ungetch("s")
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')
            self.assert_character(mygame, 0, 1, '#')

    def testPlayerMovementRestrictionUp(self):
        m = world_map.WorldMap(mapstr="#\n@")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '#')
            self.assert_character(mygame, 0, 1, '@')
            curses.ungetch("w")
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '#')
            self.assert_character(mygame, 0, 1, '@')

    def testWalkBack(self):
        m = world_map.WorldMap(mapstr='@')
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')
            curses.ungetch("d")
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, ' ')
            self.assert_character(mygame, 1, 0, '@')
            curses.ungetch("a")
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')
            self.assert_character(mygame, 1, 0, ' ')

    def testMoveOffLeftStillOnMap(self):
        m = world_map.WorldMap(mapstr='@#')
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')
            self.assert_character(mygame, 1, 0, '#')

            curses.ungetch("a")
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, ' ')
            self.assert_character(mygame, 1, 0, '#')

    def testMoveOnUnderscore(self):
        m = world_map.WorldMap(mapstr='@\n_')
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')
            self.assert_character(mygame, 0, 1, '_')
            self.assert_does_not_have_attributes(mygame, 0, 1, curses.A_UNDERLINE)

            curses.ungetch("s")
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, ' ')
            self.assert_character(mygame, 0, 1, '@')
            self.assert_character(mygame, 0, 2, ' ')
            self.assert_has_attributes(mygame, 0, 1, curses.A_UNDERLINE)

            curses.ungetch("s") # We shouldn't be able to move down past the _
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, ' ')
            self.assert_character(mygame, 0, 1, '@')
            self.assert_character(mygame, 0, 2, ' ')
            self.assert_has_attributes(mygame, 0, 1, curses.A_UNDERLINE)

            curses.ungetch("d") # We should be able to move right off the _
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, ' ')
            self.assert_character(mygame, 0, 1, '_')
            self.assert_character(mygame, 1, 1, '@')
            self.assert_character(mygame, 0, 2, ' ')
            self.assert_does_not_have_attributes(mygame, 0, 1, curses.A_UNDERLINE)
            self.assert_does_not_have_attributes(mygame, 1, 1, curses.A_UNDERLINE)

    def testMoveDownFromUnderscoreToUnderscore(self):
        m = world_map.WorldMap(mapstr='@\n_\n_')
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '@')
            self.assert_character(mygame, 0, 1, '_')
            self.assert_does_not_have_attributes(mygame, 0, 1, curses.A_UNDERLINE)

            curses.ungetch("s")
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, ' ')
            self.assert_character(mygame, 0, 1, '@')
            self.assert_character(mygame, 0, 2, '_')
            self.assert_has_attributes(mygame, 0, 1, curses.A_UNDERLINE)

            curses.ungetch("s") # We shouldn't be able to move down past the _
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, ' ')
            self.assert_character(mygame, 0, 1, '@')
            self.assert_character(mygame, 0, 2, '_')
            self.assert_has_attributes(mygame, 0, 1, curses.A_UNDERLINE)

    def testMoveUpToUnderscore(self):
        m = world_map.WorldMap(mapstr='_\n@')
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '_')
            self.assert_character(mygame, 0, 1, '@')

            curses.ungetch("w")
            mygame.advance_one_frame()
            self.assert_character(mygame, 0, 0, '_')
            self.assert_character(mygame, 0, 1, '@')
