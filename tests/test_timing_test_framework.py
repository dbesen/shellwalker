import time
from mock import patch
import test_base

class TestTimingTestFramework(test_base.TestBase):

    @patch("time.time")
    def testMockTimeSingleArg(self, mockTime):
        mockTime.return_value = 0
        self.assertEquals(time.time(), 0)
        mockTime.return_value = 5
        self.assertEquals(time.time(), 5)

    @patch("time.time")
    def testMockTimeTwoArg(self, mockTime):
        mockTime.side_effect = [0, 5]
        self.assertEquals(time.time(), 0)
        self.assertEquals(time.time(), 5)

    @patch("time.sleep")
    def testExpectSleepPass(self, mockSleep):
        time.sleep(3)
        time.sleep(4)
        self.assert_sleeps(mockSleep, 3, 4)

    @patch("time.sleep")
    def testExpectSleepFail(self, mockSleep):
        time.sleep(3)
        time.sleep(5)
        with self.shouldFail():
            self.assert_sleeps(mockSleep, 3, 4)

    def testSleepGoesByQuickly(self):
        # 10 years -- so the user will surely notice a failure.
        # This is monkeypatched in test_base.py.
        time.sleep(1000 * 60 * 60 * 24 * 365 * 10)
