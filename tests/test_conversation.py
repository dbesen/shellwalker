
import test_base
from shellwalker import conversation

class TestConversation(test_base.TestBase):
    def test_empty_conversation(self):
        with self.shouldFail(type=AttributeError):
            conversation.Conversation(None)

    def test_basic_conversation(self):
        c = conversation.Conversation("message")
        self.assertEquals("message", c.get_text())
        self.assertEquals("message", c.get_text())

    def test_response(self):
        c = conversation.Conversation("message 1\n> answer\nmessage 2")
        self.assertEquals("message 1", c.get_text())
        self.assertEquals("message 1", c.get_text())
        self.assertEqual(1, c.get_answer_count())
        self.assertEqual("answer", c.get_answer(0))
        c.select_response(0)
        self.assertEquals("message 2", c.get_text())

    def test_two_answers_merge(self):
        c = conversation.Conversation("message 1\n> answer 1\n> answer 2\nmessage 2")
        self.assertEquals("message 1", c.get_text())
        self.assertEqual(2, c.get_answer_count())
        self.assertEqual("answer 1", c.get_answer(0))
        self.assertEqual("answer 2", c.get_answer(1))
        c.select_response(1)
        self.assertEquals("message 2", c.get_text())

    def test_two_answers_split_1(self):
        c = conversation.Conversation("message 1\n> answer 1\n  message 2\n> answer 2\n  message 3")
        self.assertEquals("message 1", c.get_text())
        self.assertEqual(2, c.get_answer_count())
        self.assertEqual("answer 1", c.get_answer(0))
        self.assertEqual("answer 2", c.get_answer(1))
        c.select_response(0)
        self.assertEquals("message 2", c.get_text())

    def test_two_answers_split_2(self):
        c = conversation.Conversation("message 1\n> answer 1\n  message 2\n> answer 2\n  message 3")
        self.assertEquals("message 1", c.get_text())
        self.assertEqual(2, c.get_answer_count())
        self.assertEqual("answer 1", c.get_answer(0))
        self.assertEqual("answer 2", c.get_answer(1))
        c.select_response(1)
        self.assertEquals("message 3", c.get_text())

    def test_fall_out_two_levels(self):
        c = conversation.Conversation("message 1\n> answer 1\n  message 2\n  > answer 2\nmessage 3")
        self.assertEquals("message 1", c.get_text())
        c.select_response(0)
        self.assertEquals("message 2", c.get_text())
        c.select_response(0)
        self.assertEquals("message 3", c.get_text())
