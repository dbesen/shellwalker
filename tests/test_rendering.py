# coding: utf-8
from mock import patch
from mock import Mock
import test_base

import shellwalker.game as game

class TestRendering(test_base.TestBase):

    def test_clear_each_frame(self):
        with game.Game() as mygame:
            mygame.render(1, 1, "a")
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 1, " ")

    def test_game_renders_frames_in_sequence(self):
        num_frames_to_check = 10
        with game.Game() as mygame:
            self.num_times_called = 0
            def callback():
                self.num_times_called = self.num_times_called + 1
                if self.num_times_called >= num_frames_to_check:
                    mygame.pause()
            mygame.register_callback('frame', callback)
            mygame.play()
            self.assertEquals(num_frames_to_check, self.num_times_called)

    def test_callbacks_called_during_advance(self):
        with game.Game() as mygame:
            self.num_times_called = 0
            def callback():
                self.num_times_called = self.num_times_called + 1
            mygame.register_callback('frame', callback)
            mygame.advance_one_frame()
        self.assertEquals(1, self.num_times_called)

    @patch("time.time")
    def test_frame_times_passed_to_callback_slow(self, mockTime):
        mockTime.side_effect = [10, 30, 55]
        with game.Game() as mygame:
            self.assertEquals(0, mygame.get_elapsed_since_last_frame())
            self.assertEquals(0, mygame.get_time_since_start())
            mygame.advance_one_frame()
            self.assertEquals(20, mygame.get_elapsed_since_last_frame())
            self.assertEquals(20, mygame.get_time_since_start())
            mygame.advance_one_frame()
            self.assertEquals(25, mygame.get_elapsed_since_last_frame())
            self.assertEquals(45, mygame.get_time_since_start())

    @patch("time.time")
    @patch("time.sleep")
    def test_frame_times_passed_to_callback_fast(self, mockSleep, mockTime):
        mockTime.side_effect = [0, .0003, game.TIME_PER_FRAME + .0004, (2 * game.TIME_PER_FRAME) + .0002]
        with game.Game() as mygame:
            self.assertEquals(0, mygame.get_elapsed_since_last_frame())
            self.assertEquals(0, mygame.get_time_since_start())
            mygame.advance_one_frame()
            self.assertEquals(game.TIME_PER_FRAME, mygame.get_elapsed_since_last_frame())
            self.assertEquals(game.TIME_PER_FRAME, mygame.get_time_since_start())
            mygame.advance_one_frame()
            self.assertEquals(game.TIME_PER_FRAME, mygame.get_elapsed_since_last_frame())
            self.assertEquals(2 * game.TIME_PER_FRAME, mygame.get_time_since_start())
            self.assert_sleeps(mockSleep, game.TIME_PER_FRAME - .0003, game.TIME_PER_FRAME - .0004)

    @patch("time.time")
    @patch("time.sleep")
    def test_frame_times_passed_to_callback_slow_then_fast(self, mockSleep, mockTime):
        mockTime.side_effect = [0, 20, 20.0003]
        with game.Game() as mygame:
            self.assertEquals(0, mygame.get_elapsed_since_last_frame())
            self.assertEquals(0, mygame.get_time_since_start())
            mygame.advance_one_frame()
            self.assertEquals(20, mygame.get_elapsed_since_last_frame())
            self.assertEquals(20, mygame.get_time_since_start())
            mygame.advance_one_frame()
            self.assertEquals(game.TIME_PER_FRAME, mygame.get_elapsed_since_last_frame())
            self.assertEquals(20 + game.TIME_PER_FRAME, mygame.get_time_since_start())
            self.assert_sleeps(mockSleep, game.TIME_PER_FRAME - .0003)

    @patch("time.time")
    @patch("time.sleep")
    def test_frame_times_passed_to_callback_fast_then_slow(self, mockSleep, mockTime):
        mockTime.side_effect = [0, .0003, game.TIME_PER_FRAME + 20]
        with game.Game() as mygame:
            self.assertEquals(0, mygame.get_elapsed_since_last_frame())
            self.assertEquals(0, mygame.get_time_since_start())
            mygame.advance_one_frame()
            self.assertEquals(game.TIME_PER_FRAME, mygame.get_elapsed_since_last_frame())
            self.assertEquals(game.TIME_PER_FRAME, mygame.get_time_since_start())
            mygame.advance_one_frame()
            self.assertEquals(20, mygame.get_elapsed_since_last_frame())
            self.assertEquals(20 + game.TIME_PER_FRAME, mygame.get_time_since_start())
            self.assert_sleeps(mockSleep, game.TIME_PER_FRAME - .0003)

    def test_render_negative_no_exception(self):
        with game.Game() as mygame:
            mygame.render(-1, -1, "#")

    def test_render_large_positive_no_exception(self):
        with game.Game() as mygame:
            mygame.render(10000, 10000, "#")

    def test_render_off_right(self):
        with game.Game() as mygame:
            mygame.zero_top_left()
            mygame.stdscr = Mock(wraps=mygame.stdscr)
            mygame.stdscr.getmaxyx = Mock(return_value = (20, 5))
            mygame.render(0, 0, '0123456')
            self.assert_character(mygame, 3, 0, '3')
            self.assert_character(mygame, 4, 0, '4')
            self.assert_character(mygame, 5, 0, ' ')

    def test_render_off_bottom(self):
        with game.Game() as mygame:
            mygame.zero_top_left()
            mygame.stdscr = Mock(wraps=mygame.stdscr)
            mygame.stdscr.getmaxyx = Mock(return_value = (9, 20))
            mygame.render(0, 7, '7')
            mygame.render(0, 8, '8')
            mygame.render(0, 9, '9')
            self.assert_character(mygame, 0, 7, '7')
            self.assert_character(mygame, 0, 8, '8')
            self.assert_character(mygame, 0, 9, ' ')

    def test_render_unicode_double_width(self):
        with self.shouldFail("Impossible until Python supports in_wch"):
            with game.Game() as mygame:
                mygame.zero_top_left()
                mygame.render(0, 0, '私')
                self.assert_character(mygame, 0, 0, '私')
                self.assert_character(mygame, 1, 0, ' ') # ?
                self.assert_character(mygame, 2, 0, ' ')

    def test_render_unicode_single_width(self):
        with self.shouldFail("Impossible until Python supports in_wch"):
            with game.Game() as mygame:
                mygame.zero_top_left()
                mygame.render(0, 0, 'ｱ')
                self.assert_character(mygame, 0, 0, 'ｱ')
                self.assert_character(mygame, 1, 0, ' ')
                self.assert_character(mygame, 2, 0, ' ')

    def test_render_box_3(self):
        with game.Game() as mygame:
            mygame.zero_top_left()
            mygame.render_box(0, 0, 3, 3)
            self.assert_string(mygame, 0, 0, "/-\ ")
            self.assert_string(mygame, 0, 1, "| | ")
            self.assert_string(mygame, 0, 2, "\-/ ")
            self.assert_string(mygame, 0, 3, "    ")

    def test_render_box_offsets(self):
        with game.Game() as mygame:
            mygame.zero_top_left()
            mygame.render_box(1, 2, 4, 3)
            self.assert_string(mygame, 1, 2, "/--\ ")
            self.assert_string(mygame, 1, 3, "|  | ")
            self.assert_string(mygame, 1, 4, "\--/ ")
            self.assert_string(mygame, 1, 5, "     ")

    def test_render_box_4(self):
        with game.Game() as mygame:
            mygame.zero_top_left()
            mygame.render_box(0, 0, 4, 4)
            self.assert_string(mygame, 0, 0, "/--\ ")
            self.assert_string(mygame, 0, 1, "|  | ")
            self.assert_string(mygame, 0, 2, "|  | ")
            self.assert_string(mygame, 0, 3, "\--/ ")
            self.assert_string(mygame, 0, 4, "     ")

    def test_render_box_6_3(self):
        with game.Game() as mygame:
            mygame.zero_top_left()
            mygame.render_box(0, 0, 6, 3)
            self.assert_string(mygame, 0, 0, "/----\ ")
            self.assert_string(mygame, 0, 1, "|    | ")
            self.assert_string(mygame, 0, 2, "\----/ ")
            self.assert_string(mygame, 0, 3, "       ")

    def test_render_textbox_empty(self):
        with game.Game() as mygame:
            mygame.zero_top_left()
            mygame.render_textbox(0, 0, "")
            self.assert_string(mygame, 0, 0, "/--\ ")
            self.assert_string(mygame, 0, 1, "\--/ ")
            self.assert_string(mygame, 0, 2, "       ")

    def test_render_textbox_one_line(self):
        with game.Game() as mygame:
            mygame.zero_top_left()
            mygame.render_textbox(0, 0, "hi")
            self.assert_string(mygame, 0, 0, "/----\ ")
            self.assert_string(mygame, 0, 1, "| hi | ")
            self.assert_string(mygame, 0, 2, "\----/ ")
            self.assert_string(mygame, 0, 3, "       ")

    def test_render_textbox_one_line_longer(self):
        with game.Game() as mygame:
            mygame.zero_top_left()
            mygame.render_textbox(0, 0, "hello world")
            self.assert_string(mygame, 0, 0, "/-------------\ ")
            self.assert_string(mygame, 0, 1, "| hello world | ")
            self.assert_string(mygame, 0, 2, "\-------------/ ")
            self.assert_string(mygame, 0, 3, "                ")

    def test_render_textbox_two_lines(self):
        with game.Game() as mygame:
            mygame.zero_top_left()
            mygame.render_textbox(0, 0, "a\nbc")
            self.assert_string(mygame, 0, 0, "/----\ ")
            self.assert_string(mygame, 0, 1, "| a  | ")
            self.assert_string(mygame, 0, 2, "| bc | ")
            self.assert_string(mygame, 0, 3, "\----/ ")
            self.assert_string(mygame, 0, 4, "       ")

    def test_render_textbox_line_wrap(self):
        with game.Game() as mygame:
            mygame.zero_top_left()
            mygame.render_textbox(0, 0, "hello world", maxwidth=7)
            self.assert_string(mygame, 0, 0, "/-------\ ")
            self.assert_string(mygame, 0, 1, "| hello | ")
            self.assert_string(mygame, 0, 2, "| world | ")
            self.assert_string(mygame, 0, 3, "\-------/ ")
            self.assert_string(mygame, 0, 4, "          ")

    def test_render_cleanup(self):
        ''' Something should clean up between tests, so the game should be empty at the beginning of each. '''
        with game.Game() as mygame:
            mygame.zero_top_left()
            mygame.render(0, 0, "1234")
            self.assert_string(mygame, 0, 0, "1234")
        with game.Game() as mygame:
            mygame.zero_top_left()
            self.assert_string(mygame, 0, 0, "@   ")
            self.assert_string(mygame, 0, 1, "    ")
            self.assert_string(mygame, 0, 2, "    ")
