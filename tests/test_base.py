import unittest2
import mock
import time

class TestBase(unittest2.TestCase):

    def setUp(self):
        self.original_sleep = time.sleep
        time.sleep = mock.Mock() # for speed

    def tearDown(self):
        time.sleep = self.original_sleep

    def assert_sleeps(self, mockSleep, *args):
        for expected, actual in zip(args, mockSleep.call_args_list):
            self.assertAlmostEqual(expected, actual[0][0])

    def assert_game_is_not_running(self, game):
        self.assertFalse(game.isRunning())

    def assert_game_is_running(self, game):
        self.assertTrue(game.isRunning())

    def assert_character(self, game, x, y, char):
        self.assertEquals(game.get_character(x, y), char)

    def assert_has_attributes(self, game, x, y, expected_attrs):
        ''' The character may have other attributes not listed in expected_attrs. '''
        ch = game.get_raw_character(x, y)
        self.assertEqual(expected_attrs, ch & expected_attrs)

    def assert_does_not_have_attributes(self, game, x, y, attrs):
        ch = game.get_raw_character(x, y)
        self.assertEqual(0, ch & attrs)

    def assert_string(self, game, x, y, str):
        self.assertEquals(game.get_string(x, y, len(str)), str)

    def assertInstanceOf(self, expectedClass, item):
        self.assertEquals(expectedClass.__name__, item.__class__.__name__)

    def assertNotCalled(self, mock):
        self.assertEquals(None, mock.call_args)

    def assertNotNone(self, ob):
        with self.shouldFail('Expected not None, but was None'):
            self.assertEquals(ob, None)

    def assertSame(self, a, b):
        self.assertTrue(a is b)

    def shouldFail(self, msg="Expected a failure", type=AssertionError):
        class should_fail(object):
            def __enter__(self):
                pass

            def __exit__(self, got_type, value, traceback):
                if got_type is type:
                    return True
                raise AssertionError(msg)
        return should_fail()

    def assert_contains(self, haystack, needle):
        self.assertTrue(needle in haystack, "Did not find '%s' in '%s'" % (needle, haystack))
