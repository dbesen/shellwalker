import test_base
from shellwalker import world_map, game, teleport_manager, coordinate
import curses

class TestTeleporting(test_base.TestBase):

    def testTeleportDisplay(self):
        m = world_map.WorldMap()
        m.loadFromString(" @\n[3] [3]")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 0, '@')
            self.assert_character(mygame, 1, 1, ' ')
            self.assert_character(mygame, 5, 1, ' ')

    def testTeleportDisplayDifferent(self):
        m = world_map.WorldMap()
        m.loadFromString(" @\n[a] [a]")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 0, '@')
            self.assert_character(mygame, 1, 1, ' ')
            self.assert_character(mygame, 5, 1, ' ')

    def testBasicTeleport(self):
        m = world_map.WorldMap()
        m.loadFromString(" @\n[3] [3]")
        with game.Game(map=m) as mygame:
            mygame.zero_top_left()
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 0, '@')
            self.assert_character(mygame, 1, 1, ' ')
            self.assert_character(mygame, 5, 1, ' ')
            curses.ungetch("s")
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 0, ' ')
            self.assert_character(mygame, 1, 1, ' ')
            self.assert_character(mygame, 5, 1, '@')
            # Make sure we don't teleport back immediately
            mygame.advance_one_frame()
            self.assert_character(mygame, 1, 1, ' ')
            self.assert_character(mygame, 5, 1, '@')

    def testPlayerStaysStillAfterTeleport(self):
        m = world_map.WorldMap()
        m.loadFromString(" @\n[3] [3]")
        with game.Game(map=m) as mygame:
            mygame.advance_one_frame()
            player_render_x_before = mygame.player.x - mygame.viewport.x
            player_render_y_before = mygame.player.y - mygame.viewport.y

            curses.ungetch("s")
            mygame.advance_one_frame()

            player_render_x_after = mygame.player.x - mygame.viewport.x
            player_render_y_after = mygame.player.y - mygame.viewport.y

            self.assertEquals(player_render_x_before, player_render_x_after)
            self.assertEquals(player_render_y_before, player_render_y_after)

    def testCoordinate(self):
        i = world_map.IndividualMap("")
        c = coordinate.Coordinate(i, 1, 2)
        self.assertSame(i, c.map)
        self.assertEquals(1, c.x)
        self.assertEquals(2, c.y)
        self.assert_contains(str(c), "oord")

    def assertTeleportsTo(self, tm, c1, c2):
        self.assertEquals(c2.x, tm.teleports_to(c1).x)
        self.assertEquals(c2.y, tm.teleports_to(c1).y)
        self.assertSame(c1.map, tm.teleports_to(c2).map)

    def testTeleportManagerSimpleCase(self):
        tm = teleport_manager.TeleportManager()
        i = world_map.IndividualMap("")
        c1 = coordinate.Coordinate(i, 0, 1)
        c2 = coordinate.Coordinate(i, 2, 3)
        tm.add_teleport('2', c1)
        tm.add_teleport('2', c2)

        self.assertTeleportsTo(tm, c1, c2)
        self.assertTeleportsTo(tm, c2, c1)

    def testTeleportManagerDifferentTeleporters(self):
        tm = teleport_manager.TeleportManager()
        i = world_map.IndividualMap("")
        c1 = coordinate.Coordinate(i, 0, 1)
        c2 = coordinate.Coordinate(i, 2, 3)

        c3 = coordinate.Coordinate(i, 4, 5)
        c4 = coordinate.Coordinate(i, 6, 7)

        tm.add_teleport('2', c1)
        tm.add_teleport('2', c2)

        tm.add_teleport('3', c3)
        tm.add_teleport('3', c4)

        self.assertTeleportsTo(tm, c1, c2)
        self.assertTeleportsTo(tm, c2, c1)

        self.assertTeleportsTo(tm, c3, c4)
        self.assertTeleportsTo(tm, c4, c3)

    def testTeleportManagerDifferentMap(self):
        tm = teleport_manager.TeleportManager()
        i1 = world_map.IndividualMap(" 1")
        i2 = world_map.IndividualMap(" 2")
        c1 = coordinate.Coordinate(i1, 0, 1)
        c2 = coordinate.Coordinate(i2, 2, 3)
        tm.add_teleport('3', c1)
        tm.add_teleport('3', c2)

        self.assertTeleportsTo(tm, c1, c2)
        self.assertTeleportsTo(tm, c2, c1)

    def testTeleportManagerLoop(self):
        tm = teleport_manager.TeleportManager()
        i = world_map.IndividualMap("")
        c1 = coordinate.Coordinate(i, 0, 1)
        c2 = coordinate.Coordinate(i, 2, 3)
        c3 = coordinate.Coordinate(i, 4, 5)
        tm.add_teleport('2', c1)
        tm.add_teleport('2', c2)
        tm.add_teleport('2', c3)

        self.assertTeleportsTo(tm, c1, c2)
        self.assertTeleportsTo(tm, c2, c3)
        self.assertTeleportsTo(tm, c3, c1)

    def testTeleportManagerOnlyOneCoordinate(self):
        tm = teleport_manager.TeleportManager()
        i = world_map.IndividualMap("")
        c = coordinate.Coordinate(i, 0, 1)
        tm.add_teleport('2', c)

        self.assertTeleportsTo(tm, c, c)

    def testCoordinateDifferentObjectEquals(self):
        i = world_map.IndividualMap("")
        self.assertEquals(coordinate.Coordinate(i, 0, 1), coordinate.Coordinate(i, 0, 1))

    def testTeleportManagerDifferentObjectLookup(self):
        tm = teleport_manager.TeleportManager()
        i = world_map.IndividualMap("")
        c1 = coordinate.Coordinate(i, 0, 1)
        c2 = coordinate.Coordinate(i, 2, 3)
        tm.add_teleport('2', c1)
        tm.add_teleport('2', c2)

        self.assertTeleportsTo(tm, coordinate.Coordinate(i, 0, 1), c2)
        self.assertTeleportsTo(tm, coordinate.Coordinate(i, 2, 3), c1)

    def testTeleportManagerUnrelatedLookup(self):
        tm = teleport_manager.TeleportManager()
        i = world_map.IndividualMap("")
        c1 = coordinate.Coordinate(i, 0, 1)
        c2 = coordinate.Coordinate(i, 2, 3)
        tm.add_teleport('2', c1)
        tm.add_teleport('2', c2)

        c3 = coordinate.Coordinate(i, 4, 5)

        self.assertTeleportsTo(tm, c3, c3)
        self.assertTeleportsTo(tm, c3, c3)
