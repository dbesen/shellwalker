from setuptools import setup
import re
from pip._internal.req import parse_requirements
from pip._internal.network.session import PipSession


# I don't like having a separate file for MANIFEST.in, so we store the content here,
# and write it out dynamically.  It's in .gitignore so clean will delete it.
manifest_in_items = ["recursive-include shellwalker/maps *",
    "include readme.txt",
    "include pip-requires",
    ]

with open("MANIFEST.in", "w") as f:
    for line in manifest_in_items:
        f.write(line)
        f.write("\n")

with open("readme.txt") as f:
    readme_text = f.read()

install_reqs = parse_requirements("pip-requires", session=PipSession())

reqs = [str(ir.req) for ir in install_reqs]

def get_version():
    for line in readme_text.splitlines():
        matches = re.match('Shellwalker v(\S+)', line)
        if matches:
            return matches.group(1)
    return "undefined"

setup(name='shellwalker',
    description='A terminal-based video game',
    long_description=readme_text,
    url='https://bitbucket.org/dbesen/shellwalker',
    version=get_version(),
    packages=['shellwalker'],
    scripts=['bin/shellwalker'],
    include_package_data=True,
    author='David Besen',
    author_email='dbesen@gmail.com',
    install_requires=reqs,
    )
